window.encodeURIComponent = function (value) {
  var map = {
    ' ': '%20',
    '#': '%23',
    '%': '%25',
    '&': '%26',
    '+': '%2B',
    '/': '%2F',
    '\'': '\'\'', // double for wio informix sql insert
    '=': '%3D',
    '?': '%3F',
    '\r': '%0D',
    '\n': '%0A'
  };

  return value.replace(/[ #%&+/=''?\r\n]/g, function (c) {
    return map[c];
  });
};

(function () {
  window.notJSON = function (data) {
    try {
      $.parseJSON(data);
      return false;
    } catch (err) {
      return true;
    }
  }
})();



$.ajaxSetup({
  cache: false
});

//判斷網址 改變變數  網域問題
var pageURL = window.location.origin;

if (pageURL == "http://eorder.howtobe.com.tw") {

  var domain = 'http://eorder.howtobe.com.tw/scmdb/login/crmd010w_api.html?';

} else {

  var domain = 'http://baoma.gfc.com.tw/scmdb/login/crmd010w_api.html?';
};




var getUrlParameter = function getUrlParameter(sParam) {
  var sPageURL = window.location.search.substring(1),
    sURLVariables = sPageURL.split('&'),
    sParameterName,
    i;
  for (i = 0; i < sURLVariables.length; i++) {
    sParameterName = sURLVariables[i].split('=');

    if (sParameterName[0] === sParam) {
      return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
    }
  }
};

function myBrowser() {
  //取得瀏覽器的userAgent字串
  var userAgent = navigator.userAgent;
  var isOpera = userAgent.indexOf("Opera") > -1;
  //判斷是否Opera瀏覽器
  if (isOpera) {
    return "Opera"
  };
  //判斷是否Firefox瀏覽器
  if (userAgent.indexOf("Firefox") > -1) {
    return "FF";
  };
  //判斷是否Chrome瀏覽器
  if (userAgent.indexOf("Chrome") > -1) {
    return "Chrome";
  };
  //判斷是否Safari瀏覽器
  if (userAgent.indexOf("Safari") > -1) {
    return "Safari";
  };
  //判斷是否IE瀏覽器 
  if (userAgent.indexOf("compatible") > -1 && userAgent.indexOf("MSIE") > -1 && !isOpera) {
    return "IE";
  };
};

//驗證碼設定


function change_code() {
  var captcha = new $.Captcha({
    onFailure: function () {
      $("#checknum").val("");
      $("#checknum").focus();
    },
    onSuccess: function () {
      // alert("CORRECT!!!");
    }
  });
  captcha.generate();
};

//判斷有樓層的選項被選取時 樓層未選取
function floor_judge() {
  var test12 = $('#test12').prop('checked');
  var err_f_safe = $("#err_f_safe").val();
  var test3 = $('#test3').prop('checked');
  var err_f_door = $("#err_f_door").val();
  var test5 = $('#test5').prop('checked');
  var err_f_button = $("#err_f_button").val();
  var test7 = $('#test7').prop('checked');
  var err_f_floor = $("#err_f_floor").val();
  var test9 = $('#test9').prop('checked');
  var err_f_car = $("#err_f_car").val();
  var test11 = $('#test11').prop('checked');
  var err_f_walk = $("#err_f_walk").val();
  if (test12 && !err_f_safe) {
    Swal.fire({
      html: '<h3 style=color:red;>到樓不平樓層未選</h3>',
      showConfirmButton: false,
      timer: 1000
    });
    setTimeout(function () {
      $('html,body').animate({
        scrollTop: $('#checkbox_list').offset().top
      }, 100);
    }, 1500);
    $("em").html("");
  } else if (test3 && !err_f_door) {
    Swal.fire({
      html: '<h3 style=color:red;>門開關異常樓層未選</h3>',
      showConfirmButton: false,
      timer: 1000
    });
    setTimeout(function () {
      $('html,body').animate({
        scrollTop: $('#checkbox_list').offset().top
      }, 100);
    }, 1500);
    $("em").html("");
  } else if (test5 && !err_f_button) {
    Swal.fire({
      html: '<h3 style=color:red;>按鈕異常樓層未選</h3>',
      showConfirmButton: false,
      timer: 1000
    });
    setTimeout(function () {
      $('html,body').animate({
        scrollTop: $('#checkbox_list').offset().top
      }, 100);
    }, 1500);
    $("em").html("");
  } else if (test7 && !err_f_floor) {
    Swal.fire({
      html: '<h3 style=color:red;>樓層顯示異常樓層未選</h3>',
      showConfirmButton: false,
      timer: 1000
    });
    setTimeout(function () {
      $('html,body').animate({
        scrollTop: $('#checkbox_list').offset().top
      }, 100);
    }, 1500);
    $("em").html("");
  } else if (test9 && !err_f_car) {
    Swal.fire({
      html: '<h3 style=color:red;>車行異音樓層未選</h3>',
      showConfirmButton: false,
      timer: 1000
    });
    setTimeout(function () {
      $('html,body').animate({
        scrollTop: $('#checkbox_list').offset().top
      }, 100);
    }, 1500);
    $("em").html("");
  } else if (test11 && !err_f_walk) {
    Swal.fire({
      html: '<h3 style=color:red;>行走抖動樓層未選</h3>',
      showConfirmButton: false,
      timer: 1000
    });
    setTimeout(function () {
      $('html,body').animate({
        scrollTop: $('#checkbox_list').offset().top
      }, 100);
    }, 1500);
    $("em").html("");
  } else {
    $("em").html("");
  }
};


$(document).ready(function () {

  $('#reflection').prop('disabled', true);

  $('#no_agree').hide();


  //驗證碼更改
  change_code();


  //判斷網址 改變字體 有跨域問題  
  if (pageURL == "http://eorder.howtobe.com.tw") {
    $("body").append("<style>@font-face {font-family: 'one-title'; src: url('http://eorder.howtobe.com.tw/crmd010w/fonts/AdobeFanHeitiStd-Bold2.otf'),url('http://eorder.howtobe.com.tw/crmd010w/fonts/AdobeFanHeitiStd-Bold.woff') format('woff');}</style>");
  } else {
    $("body").append("<style>@font-face {font-family: 'one-title'; src: url('http://baoma.gfc.com.tw/crmd010w/fonts/AdobeFanHeitiStd-Bold2.otf'),url('http://baoma.gfc.com.tw/crmd010w/fonts/AdobeFanHeitiStd-Bold.woff') format('woff');}</style>");
  }

  //判斷目前用何種瀏覽器瀏覽
  var mb = myBrowser();
  if ("IE" == mb) {
    console.log("IE");
  };
  if ("FF" == mb) {
    console.log("Firefox");
  };
  if ("Chrome" == mb) {
    console.log("Chrome");
  };
  if ("Opera" == mb) {
    console.log("Opera");
  };
  if ("Safari" == mb) {
    console.log("Safari");
  };



  //點選其他配合事項的判斷
  var other_seleted = function () {
    if ($("#test10").is(":checked")) {
      Swal.fire({
        html: '<h3 style=color:red;>請填補充說明</h3>',
        showConfirmButton: false,
        timer: 1000
      });
      setTimeout(function () {
        $('#apply_desc').val("");
        $('#apply_desc').focus();
      }, 1500);
    } else {
      $('#apply_desc').val("無");

    }
  };

  $(other_seleted);
  $("#test10").change(other_seleted);



  //spin的動畫 讀完跳出是否選項
  // Swal.fire({
  //   // title: data,
  //   html: '<center><div class="spin" data-spin style="display: inline"></div></center>',
  //   showConfirmButton: false,
  //   background: 'url(img/mem/true3.jpg)',
  // })
  Swal.fire({
    // title: data,
    html: '<img src="http://eorder.howtobe.com.tw/crmd010w/css/images/spin.gif" >',
    showConfirmButton: false,
    background: 'url(img/mem/true3.jpg)',
  })



  //當有樓層選項未被選取  樓層下拉選單不得點選
  var update_door = function () {
    if ($("#test3").is(":checked")) {
      $('#err_f_door').attr('disabled', false);
    } else {
      $('#err_f_door').attr('disabled', true);
    }
  };

  $(update_door);
  $("#test3").change(update_door);


  var update_button = function () {
    if ($("#test5").is(":checked")) {
      $('#err_f_button').attr('disabled', false);
    } else {
      $('#err_f_button').attr('disabled', true);
    }
  };

  $(update_button);
  $("#test5").change(update_button);


  //新增樓不平樓層/叫車無效樓層
  var update_floor = function () {
    if ($("#test7").is(":checked")) {
      $('#err_f_floor').attr('disabled', false);
    } else {
      $('#err_f_floor').attr('disabled', true);
    }
  };

  $(update_floor);
  $("#test7").change(update_floor);


  var update_car = function () {
    if ($("#test9").is(":checked")) {
      $('#err_f_car').attr('disabled', false);
    } else {
      $('#err_f_car').attr('disabled', true);
    }
  };

  $(update_car);
  $("#test9").change(update_car);


  //test12

  var update_safe = function () {
    if ($("#test12").is(":checked")) {
      $('#err_f_safe').attr('disabled', false);
    } else {
      $('#err_f_safe').attr('disabled', true);
    }
  };

  $(update_safe);
  $("#test12").change(update_safe);

  //test11

  var update_walk = function () {
    if ($("#test11").is(":checked")) {
      $('#err_f_walk').attr('disabled', false);
    } else {
      $('#err_f_walk').attr('disabled', true);
    }
  };

  $(update_walk);
  $("#test11").change(update_walk);



  $.getJSON("https://ipv4.jsonip.com/?callback=?", function (data) {
    $("#apply_ip").val(data.ip);
  });

  var item = getUrlParameter("item");
  var ct_no = getUrlParameter("ct_no");
  var elev_no = getUrlParameter("elev_no");


  // var id = getUrlParameter("x");
  // var str = String.format('x={0}', id);
  var str = String.format('item={0}&ct_no={1}&elev_no={2}', item, ct_no, elev_no);
  var url = domain + 't=option&';
  url = String.format('{0}{1}', url, str);
  $.get(url).done(function (data) {
    if (notJSON(data)) {
      return;
    }
    var json = $.parseJSON(data);

    var item = json.item;
    var ct_no = json.ct_no;
    var elev_no = json.elev_no;



    if (item && ct_no && elev_no) {

      //董事長版
      $("#report_tel").html(json.report_tel);
      $('input:checkbox:checked[name="err_code"]').prop("checked", false);
      // $('#test4,#test5').prop("checked", false);
      $('#err_f_door,#err_f_button,#err_f_floor,#err_f_car').attr('disabled', true);
      $('#not_com_msg').html("");
      $('#reflection').prop('disabled', false);
      if (json.data) {
        // var json_length = Object.keys(json.data).length
        // var i;
        // for (i = 0; i < json_length; i++) {
        //     console.log(json.data[i]);
        //     $('#test' + json.data[i]).attr("disabled", true);
        //     $('#err_code_' + json.data[i]).attr('style', 'color:gray');
        // };
        if (json.err_f) {
          $.each(json.err_f, function (key, value) {
            $('.floor').append($("<option></option>").attr("value", value).text(value + "樓"));
          });
        }


        $.each(json.data, function (key, value) {
          var str = String(value);
          var item = str.split("_");
          var val = item[0];
          var err_f = item[1];

          // console.log(val + "_" + err_f);
          //for 10位數

          if (val == 12 && err_f != "") {
            // $('#err_f_door').attr('disabled', true);
            $('#err_f_safe option[value=' + err_f + ']').remove();
            $('#test' + val).prop("checked", false);
            // $('#test' + val).attr("disabled", true);
            $('#err_code_' + val).attr('style', 'color:gray');
          } else if (val == 3 && err_f != "") {
            // $('#err_f_door').attr('disabled', true);
            $('#err_f_door option[value=' + err_f + ']').remove();
            $('#test' + val).prop("checked", false);
            // $('#test' + val).attr("disabled", true);
            $('#err_code_' + val).attr('style', 'color:gray');
          } else if (val == 5 && err_f != "") {
            // $('#err_f_button').attr('disabled', true);
            $('#err_f_button option[value=' + err_f + ']').remove();
            $('#test' + val).prop("checked", false);
            // $('#test' + val).attr("disabled", true);
            $('#err_code_' + val).attr('style', 'color:gray');
          } else if (val == 7 && err_f != "") {
            // $('#err_f_floor').attr('disabled', true);
            $('#err_f_floor option[value=' + err_f + ']').remove();
            $('#test' + val).prop("checked", false);
            // $('#test' + val).attr("disabled", true);
            $('#err_code_' + val).attr('style', 'color:gray');
          } else if (val == 9 && err_f != "") {
            // $('#err_f_car').attr('disabled', true);
            $('#err_f_car option[value=' + err_f + ']').remove();
            $('#test' + val).prop("checked", false);
            // $('#test' + val).attr("disabled", true);
            $('#err_code_' + val).attr('style', 'color:gray');
          } else if (val == 11 && err_f != "") {
            // $('#err_f_door').attr('disabled', true);
            $('#err_f_walk option[value=' + err_f + ']').remove();
            $('#test' + val).prop("checked", false);
            // $('#test' + val).attr("disabled", true);
            $('#err_code_' + val).attr('style', 'color:gray');
          } else if (val == 10) {
            $('#test' + val).prop("checked", false);
            $('#err_code_' + val).attr('style', 'color:gray');
          } else {
            // $('#err_f_door').prop('disabled', false);
            // $('#err_f_button').prop('disabled', false);
            $('#test' + val).prop("checked", false);
            $('#test' + val).attr("disabled", true);
            $('#err_code_' + val).attr('style', 'color:gray');
          };
        });
      };
      //  else {
      //      alert('err_code Failed!');
      // };


      // Swal.fire({
      //     // title: data,
      //     html: '<h1 style=color:#F5F5DC;>讀取中...</h1>',
      //     showConfirmButton: false,
      //     background: 'url(img/mem/true2.jpg)',
      //     timer: 3000,
      // })
      setTimeout(function () {
        $('#item').html(json.item);
        $('#ct_no').html(json.ct_no);
        $('#elev_no').html(json.elev_no);
        $('#buld_name').html(json.buld_name);
        // $('#adress').html(json.adress);
        $('#not_check_msg').html("灰色字樣為已通報項目，處理中。");
      }, 500);

      // 開頭的動畫顯示//董事長版
      Swal.fire({
        html: '<img src="http://eorder.howtobe.com.tw/crmd010w/css/images/header.png" id="header_img"><br><div><a href="https://youtu.be/KDfUpimW3sA"><img src="http://eorder.howtobe.com.tw/crmd010w/css/images/why2.png" style="width:70%;" ></a></div><br><img src="http://eorder.howtobe.com.tw/crmd010w/css/images/video1.gif" style="width:80%;" >',
        showConfirmButton: true,
        confirmButtonText: '<span style="text-decoration:underline; id="err"">電梯異常直接通報</span>'
      }).then(function (result) {
        Swal.fire({
          html: '<img src="http://eorder.howtobe.com.tw/crmd010w/css/images/header.png" id="header_img"><br><span style="color: gray;">灰色字樣為已通報項目。</span>',
          showConfirmButton: true

        });
      });
      //新增提示灰色字樣


    } else {
      //董事長版本  如果非保養合約 抓5秒再跳出
      setTimeout(function () {
        Swal.fire({
          html: '<h3 style=color:red;>該合約目前非原廠保養，若有需要請電聯本公司</h3>'
        });
        $('#not_com_msg').html("該合約目前非原廠保養，若有需要請電聯本公司");
        // $('#reflection').prop('disabled', true);
        $('#apply_name,#apply_tel,#apply_mail').attr('disabled', true);
        $('#no_contract').hide();
      }, 5000);
    };

  });


  // $('#apply_desc').val("無");


  $.get(domain + 't=get_err').done(function (data) {
    if (notJSON(data)) {
      return;
    }
    var json = $.parseJSON(data);

    if (json) {
      // var err_code = Object.values(json)
      // var err_code = Object.keys(json).map(function (e) {
      //   return json[e]
      // });
      // var i;
      // for (i = 1; i <= json_length; i++) {
      //   $('#err_code_' + i).html(err_code[i]);
      //   console.log(err_code[i])
      // };
      $.each(json, function (key, value) {
        var num = key.split("_");
        $('#err_code_' + num[2]).html(value);
      });
      // $('#err_code_12').html(err_code[10]);
      // $('#err_code_1').html(err_code[11]);
      // $('#err_code_3').html(err_code[1]);
      // $('#err_code_4').html(err_code[2]);
      // $('#err_code_5').html(err_code[3]);
      // $('#err_code_6').html(err_code[4]);
      // $('#err_code_7').html(err_code[5]);
      // $('#err_code_8').html(err_code[6]);
      // $('#err_code_9').html(err_code[7]);
      // $('#err_code_2').html(err_code[0]);
      // $('#err_code_11').html(err_code[9]);
      // $('#err_code_10').html(err_code[8]);

    } else {
      alert('err_code Failed!');
    };
  });





  $("#cancel").click(function () {

    Swal.fire({
      // title: data,
      html: '<h1 style=color:#F5F5DC;>清除欄位中...</h1>',
      showConfirmButton: false,
      background: 'url(img/mem/true3.jpg)',
      timer: 1000
    })
    setTimeout(function () {
      $("input[type='text']").val("");
      $("input[type='checkbox']").prop('checked', false);
      $("em").html("");
      $('#apply_desc').val("無");
      $(update_door);
      $("#test3").change(update_door);
      $(update_button);
      $("#test5").change(update_button);
      $(update_floor);
      $("#test7").change(update_floor);
      $(update_car);
      $("#test9").change(update_car);
      //新增 11/ 12
      $(update_safe);
      $("#test12").change(update_safe);
      $(update_walk);
      $("#test11").change(update_walk);
      $('.floor').val("");
      $('html,body').animate({
        scrollTop: $('#id-wrapper').offset().top
      }, 100);
    }, 1000);

  });

  $("#list").click(function () {
    $('html,body').animate({
      scrollTop: $('#id-wrapper').offset().top
    }, 100);

    Swal.fire({
      title: '請輸入手機號碼',
      input: 'text',
      inputAttributes: {
        autocapitalize: 'off'
      },
      showCancelButton: true,
      confirmButtonText: '查看',
      cancelButtonText: '取消',
    }).then(function (result) {
      if (result.value) {

        var item = $("#item").html();
        var ct_no = $("#ct_no").html();
        var elev_no = $("#elev_no").html();
        var apply_tel_list = result.value;


        var str = String.format('&item={0}&ct_no={1}&elev_no={2}&apply_tel={3}', item, ct_no, elev_no, apply_tel_list);
        var url = domain + 't=option_list&';
        url = String.format('{0}{1}', url, str);
        $.get(url).done(function (data) {
          if (notJSON(data)) {
            return;
          }
          var json = $.parseJSON(data);

          var count = Object.keys(json.data).length;

          var i;
          var j;
          var value = "";

          for (i = 0; i < count; i++) {
            for (j = 0; j < 5; j++) {
              if (j == 0) {
                value += '<tr><td style="font-size:medium;">' + json.data[i][j] + '</td>';
              } else if (j == 1) {
                value += '<td class="option_list"  style="font-size:medium;">' + json.data[i][j] + '</td>';
              } else if (j == 2) {
                value += '<td   style="font-size:medium;">' + json.data[i][j] + '</td>';
              } else if (j == 3) {
                value += '<td class="option_list_status"  style="font-size:medium;">' + json.data[i][j] + '</td>';
              } else {
                value += '<td class="option_list_finish_date"  style="font-size:medium;">' + json.data[i][j] + "</td></tr>";
              };
            }
          };

          // console.log(value);
          Swal.fire({
            // title: data,
            html: '<img src="http://eorder.howtobe.com.tw/crmd010w/css/images/spin.gif" >',
            showConfirmButton: false,
            background: 'url(img/mem/true3.jpg)',
          })


          if (json) {
            Swal.fire({
              html: '<center><h3><報馬仔最新回報></h3><table><tr style="font-size:large;"><th>通報時間</th><th>項目</th><th>說明</th><th>進度</th><th>完成</th><tr/>' + value + '</table></center>'
            });
            // alert("[報馬仔前五筆資訊]\n[時間] [說明] [進度] [完成日]\n" + value);
          } else {
            alert('item Failed!');
          };
        });

      };
    });



    // var apply_tel = $("#apply_tel").val();

    // if (!apply_tel) {
    //   Swal.fire({
    //     // title: data,
    //     html: '<h3>請輸入電話，顯示回報之紀錄</h3>',
    //     showConfirmButton: false,
    //     timer: 1000
    //   })
    //   setTimeout(function () {
    //     $('#apply_tel').focus();
    //   }, 1500);
    // } else {


    // };
  });

  // var wow = new WOW({
  //     boxClass: 'wow',
  //     animateClass: 'animated',
  //     offset: 0,
  //     mobile: true,
  //     live: true,
  //     callback: function (box) {},
  //     scrollContainer: null
  // });
  // wow.init();


  $("#reflection").click(function (event) {
    var no_agree = $('#no_agree').css('display');
    var checknum = $("#checknum").val();
    var checkbox = new Array();
    $('input:checkbox:checked[name="err_code"]').each(function (i) {
      checkbox[i] = this.value;
    });
    if (checkbox == "") {
      $('html,body').animate({
        scrollTop: $('#id-wrapper').offset().top
      }, 100);
    }

    //當姓名電話欄位不顯示且驗證碼非空值時且選項有選取狀態 進入是否判斷
    if (no_agree == "none" && checknum != "" && checkbox != "") {
      //取消預設的submit動作
      event.preventDefault();
      Swal.fire({
        html: '<img src="http://eorder.howtobe.com.tw/crmd010w/css/images/header.png" id="header_img"><br><div align="center"><div><img  src="http://eorder.howtobe.com.tw/crmd010w/css/images/horse.gif"></div>是否需要回報處理資訊 ? <br>請於下方選擇<br><span style=color:#3085d6;>是(請填姓名/電話)</span>或<span style=color:red;>否</span>，<br>感謝您的配合。</div>',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: '是',
        cancelButtonText: '否'
      }).then(function (result) {
        //選擇留下姓名/電話
        if (result.value) {
          Swal.fire({
            title: '親愛的用戶您好:',
            html: '<div align="center"><span style="font-size:20px;">您所留之姓名，電話等個資，只用來回報電梯異常處理，無其他用途。<br>我已閱讀並同意個資告知函</span></div>',
            showConfirmButton: true,
            confirmButtonText: '我同意',
          }).then(function (result) {
            if (result.value) {
              //顯示姓名/電話欄位
              $('#no_agree').show();
              $('#apply_name,#apply_tel,#apply_mail').attr('disabled', false);
              $("#apply_mail").val(" ");
              setTimeout(function () {
                $('html,body').animate({
                  scrollTop: $('#id-wrapper').offset().top
                }, 100);
                $('#apply_name').focus();
                $('#apply_name').val(" ");
              }, 1500);
            } else {
              $("#cancel").click();
            }
          });
          //因為取消預設 照理說要直接送出
          //不要回報資料  要直接送出
        } else {
          //當不留資料時 中間紀錄紐不出現
          // $("#list").css('display', 'none');
          $("#apply_name,#apply_tel,#apply_mail").val("");
          $('#apply_name,#apply_tel,#apply_mail').attr('disabled', true);
          // $('#no_agree').hide();
          // $('#apply_name,#apply_tel,#apply_mail').attr('type', "hidden");
          floor_judge();
          $("#form").submit();
        }
      })
      //當姓名電話欄位已顯示且驗證碼非空值時 不用取消預設submit 因為會姓名/電話欄位顯示後 就進入自動判斷
    } else if (no_agree == "block") {
      var apply_name = $("#apply_name").val();
      var apply_tel = $("#apply_tel").val();
      var apply_name_err = $("#apply_name_err").html();
      if (!apply_name) {
        Swal.fire({
          html: '<h3 style=color:red;>名字未填</h3>',
          showConfirmButton: false,
          timer: 1000
        });
        setTimeout(function () {
          $('html,body').animate({
            scrollTop: $('#id-wrapper').offset().top
          }, 100);
          $('#apply_name').focus();
        }, 1500);
      } else if (!apply_tel) {
        Swal.fire({
          html: '<h3 style=color:red;>電話未填</h3>',
          showConfirmButton: false,
          timer: 1000
        });
        setTimeout(function () {
          $('html,body').animate({
            scrollTop: $('#apply_name').offset().top
          }, 100);
          $('#apply_tel').focus();
        }, 1500);
      } else if (!apply_name_err) {
        Swal.fire({
          html: '<h3 style=color:red;>手機號碼格式錯誤/號碼含空白錯誤</h3>',
          showConfirmButton: false,
          timer: 3000
        });
        setTimeout(function () {
          $('html,body').animate({
            scrollTop: $('#apply_name').offset().top
          }, 100);
          $('#apply_tel').focus();
        }, 3500);
      }
      floor_judge();

    };



  });

  //手機號碼驗證
  jQuery.validator.addMethod("isMobile", function (value, element) {
    var length = value.length;
    var mobile = /^09[0-9]{8}$/;
    return this.optional(element) || (length == 10 && mobile.test(value));
  }, "限手機號碼,範例:0912345789"); //可以自定義默認提示信息
  //新增 11/ 12
  $('#form').validate({
    focusInvalid: false,
    errorElement: "em",
    rules: {
      "apply_name": {
        required: true
        //   minlength: 2
      },
      "apply_tel": {
        required: true,
        //  minlength: 9,
        //  number: true,
        isMobile: true
      },
      "apply_mail": {
        required: true
        // email: true
      },
      "apply_desc": {
        required: true
      },
      "err_code": {
        required: true
      },
      "err_f_door": {
        required: true
      },
      "err_f_button": {
        required: true
      },
      "checknum": {
        required: true,
        minlength: 4
      },
      "err_f_floor": {
        required: true
      },
      "err_f_car": {
        required: true
      },
      "err_f_safe": {
        required: true
      },
      "err_f_walk": {
        required: true
      }
    },
    messages: {
      "apply_name": {
        required: '*必填',
        //   minlength: '*最少2個字元'
      },
      "apply_tel": {
        required: '*必填'
        // minlength: '*最少9個字元',
        // number: '*請輸入9個數字'
      },
      "apply_mail": {
        required: '*必填'
        // email: '*請輸入正確格式'
      },
      "apply_desc": {
        required: '*最少寫 " 無 "'
      },
      "err_code": {
        required: '*最少選取一筆'
      },
      "err_f_door": {
        required: '*請選取樓層'
      },
      "err_f_button": {
        required: '*請選取樓層'
      },
      "checknum": {
        required: '*驗證碼未填或錯誤',
        minlength: '*請輸入完整驗證碼'
      },
      "err_f_floor": {
        required: '*請選取樓層'
      },
      "err_f_car": {
        required: '*請選取樓層'
      },
      "err_f_safe": {
        required: '*請選取樓層'
      },
      "err_f_walk": {
        required: '*請選取樓層'
      }
    },
    errorPlacement: function (error, element) {
      var Error_msg = $(element).data('error');
      if (Error_msg) {
        $(Error_msg).append(error)
      } else {
        error.insertAfter(element);
      };
    },
    submitHandler: function (form, event) {
      $('#reflection').prop('disabled', true);
      event.preventDefault();
      // spin的動畫 讀完跳出是否選項
      Swal.fire({
        // title: data,
        html: '<img src="http://eorder.howtobe.com.tw/crmd010w/css/images/spin.gif" >',
        showConfirmButton: false,
        background: 'url(img/mem/true3.jpg)',
      })
      $('html,body').animate({
        scrollTop: $('#id-wrapper').offset().top
      }, 100);
      var item = $("#item").html();
      var ct_no = $("#ct_no").html();
      var elev_no = $("#elev_no").html();
      var apply_name = $("#apply_name").val();
      apply_name = $.trim(apply_name);
      var apply_tel = $("#apply_tel").val();
      apply_tel = $.trim(apply_tel);
      var apply_mail = $("#apply_mail").val();
      var apply_ip = $("#apply_ip").val();
      var apply_desc = $("#apply_desc").val();
      // var form_data = $(form).serializeArray();
      var str = String.format('&apply_name={0}&apply_tel={1}&apply_mail={2}&item={3}&ct_no={4}&elev_no={5}&apply_ip={6}&apply_desc={7}', apply_name, apply_tel, apply_mail, item, ct_no, elev_no, apply_ip, apply_desc);
      var url = domain + 't=insert_010&';
      url = String.format('{0}{1}', url, str);

      if (item && ct_no && elev_no) {
        $.get(url).done(function (data) {
          if (notJSON(data)) {
            alert("010資料取得失敗請稍候再試")
            return;
          }
          var json_010 = $.parseJSON(data);

          if (json_010.msg) {
            Swal.fire({
              html: '<h3 style=color:red;>' + json_010.msg + '</h3>'
            });
          } else {

            var checkbox = new Array();
            $('input:checkbox:checked[name="err_code"]').each(function (i) {
              checkbox[i] = this.value;
            });




            var checkbox_length = checkbox.length;
            var i;
            var massage = ""

            if (checkbox_length != 0) {


              for (i = 1; i <= checkbox_length; i++) {

                var err_code = checkbox[i - 1];

                $('#test' + err_code).prop("checked", false);
                $('#test' + err_code).attr("disabled", true);
                $('#err_code_' + err_code).attr('style', 'color:gray');
                //新增 11/ 12
                if (err_code == 3) {
                  var err_f = $("#err_f_door").val();
                  $('#err_f_door').attr("disabled", true);
                } else if (err_code == 5) {
                  var err_f = $("#err_f_button").val();
                  $('#err_f_button').attr("disabled", true);
                } else if (err_code == 7) {
                  var err_f = $("#err_f_floor").val();
                  $('#err_f_floor').attr("disabled", true);
                } else if (err_code == 9) {
                  var err_f = $("#err_f_car").val();
                  $('#err_f_car').attr("disabled", true);
                } else if (err_code == 11) {
                  var err_f = $("#err_f_walk").val();
                  $('#err_f_walk').attr("disabled", true);
                } else if (err_code == 12) {
                  var err_f = $("#err_f_safe").val();
                  $('#err_f_safe').attr("disabled", true);
                } else {
                  var err_f = ' ';
                }

                $.get(domain + 't=insert_015&sn_no=' + json_010.sn_no + '&apply_item=' + i + '&err_code=' + err_code + '&err_f=' + err_f).done(function (data) {

                  if (notJSON(data)) {
                    alert("015資料取得失敗請稍候再試")
                    return;
                  }
                  var json_015 = $.parseJSON(data);
                  massage += json_015.msg
                  console.log(massage);
                });
              }

              $.get(domain + 't=insert_mal&sn_no=' + json_010.sn_no + '&item=' + json_010.item + '&ct_no=' + json_010.ct_no + '&elev_no=' + json_010.elev_no).done(function (data) {

                if (notJSON(data)) {
                  alert("mal資料取得失敗請稍候再試")
                  return;
                }
                var json_mal = $.parseJSON(data);
                console.log(json_mal);
              });

              Swal.fire({
                html: '<img src="http://eorder.howtobe.com.tw/crmd010w/css/images/header.png" id="header_img"><br><div align="center"><div><img  src="http://eorder.howtobe.com.tw/crmd010w/css/images/horse.gif"></div>感謝您的通報<br>立即處理中!!</div>',
                showConfirmButton: false,
                timer: 3000
              })
              // Swal.fire({
              //   // title: data,
              //   html: '<h1 style=color:#F5F5DC;>已送出</h1>',
              //   showConfirmButton: false,
              //   background: 'url(img/mem/true3.jpg)',
              //   timer: 3000
              // })
              setTimeout(function () {
                $("input[type='text']").val("");
                $("input[type='checkbox']").prop('checked', false);
                $("em").html("");
                $('#apply_desc').val("無");
                $('#no_agree').hide();
                $("em").html("");
              }, 3000);

            } else {
              Swal.fire({
                // title: data,
                html: '<h1 style=color:#F5F5DC;>回報選項處理中</h1>',
                showConfirmButton: false,
                background: 'url(img/mem/true3.jpg)',
                timer: 3000
              })
            };

          };
        });
      } else {
        Swal.fire({
          // title: data,
          html: '<h2 style=color:#F5F5DC;>請稍待,合約讀取中...</h2>',
          showConfirmButton: false,
          background: 'url(img/mem/true3.jpg)',
          timer: 2000
        });
      };

      $('#reflection').prop('disabled', false);
    }


  });







});