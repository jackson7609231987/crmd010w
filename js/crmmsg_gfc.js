window.encodeURIComponent = function (value) {
  var map = {
    ' ': '%20',
    '#': '%23',
    '%': '%25',
    '&': '%26',
    '+': '%2B',
    '/': '%2F',
    '\'': '\'\'', // double for wio informix sql insert
    '=': '%3D',
    '?': '%3F',
    '\r': '%0D',
    '\n': '%0A'
  };

  return value.replace(/[ #%&+/=''?\r\n]/g, function (c) {
    return map[c];
  });
};

(function () {
  window.notJSON = function (data) {
    try {
      $.parseJSON(data);
      return false;
    } catch (err) {
      return true;
    }
  }
})();



$.ajaxSetup({
  cache: false
});

//判斷網址 改變變數  網域問題
var pageURL = window.location.origin;

if (pageURL == "http://eorder.howtobe.com.tw") {

  var domain = 'http://eorder.howtobe.com.tw/scmdb/login/crmd010w_api.html?';

} else {

  var domain = 'http://baoma.gfc.com.tw/scmdb/login/crmd010w_api.html?';
};




var getUrlParameter = function getUrlParameter(sParam) {
  var sPageURL = window.location.search.substring(1),
    sURLVariables = sPageURL.split('&'),
    sParameterName,
    i;
  for (i = 0; i < sURLVariables.length; i++) {
    sParameterName = sURLVariables[i].split('=');

    if (sParameterName[0] === sParam) {
      return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
    }
  }
};

function myBrowser() {
  //取得瀏覽器的userAgent字串
  var userAgent = navigator.userAgent;
  var isOpera = userAgent.indexOf("Opera") > -1;
  //判斷是否Opera瀏覽器
  if (isOpera) {
    return "Opera"
  };
  //判斷是否Firefox瀏覽器
  if (userAgent.indexOf("Firefox") > -1) {
    return "FF";
  };
  //判斷是否Chrome瀏覽器
  if (userAgent.indexOf("Chrome") > -1) {
    return "Chrome";
  };
  //判斷是否Safari瀏覽器
  if (userAgent.indexOf("Safari") > -1) {
    return "Safari";
  };
  //判斷是否IE瀏覽器 
  if (userAgent.indexOf("compatible") > -1 && userAgent.indexOf("MSIE") > -1 && !isOpera) {
    return "IE";
  };
};


$(document).ready(function () {

  //判斷網址 改變字體 有跨域問題  
  if (pageURL == "http://eorder.howtobe.com.tw") {
    $("body").append("<style>@font-face {font-family: 'one-title'; src: url('http://eorder.howtobe.com.tw/crmd010w/fonts/AdobeFanHeitiStd-Bold2.otf'),url('http://eorder.howtobe.com.tw/crmd010w/fonts/AdobeFanHeitiStd-Bold.woff') format('woff');}</style>");
  } else {
    $("body").append("<style>@font-face {font-family: 'one-title'; src: url('http://baoma.gfc.com.tw/crmd010w/fonts/AdobeFanHeitiStd-Bold2.otf'),url('http://baoma.gfc.com.tw/crmd010w/fonts/AdobeFanHeitiStd-Bold.woff') format('woff');}</style>");
  }


  var mb = myBrowser();
  if ("IE" == mb) {
    console.log("IE");
  };
  if ("FF" == mb) {
    console.log("Firefox");
  };
  if ("Chrome" == mb) {
    console.log("Chrome");
  };
  if ("Opera" == mb) {
    console.log("Opera");
  };
  if ("Safari" == mb) {
    console.log("Safari");
  }



  // var apply_tel = getUrlParameter("telno");

  var sn = getUrlParameter("sn");

  //簡訊回報訊息連結網頁內容 送到後端的apply_tel多加個0 

  // Swal.fire({
  //   // title: data,
  //   title: '查詢中',
  //   html: '<img src="http://eorder.howtobe.com.tw/crmd010w/css/images/spin.gif" >',
  //   showConfirmButton: false,
  //   background: 'url(img/mem/true3.jpg)',
  // })


  if (sn) {
    var str = String.format('sn={0}', sn);
    var url = domain + 't=option_msg_gfc&';
    url = String.format('{0}{1}', url, str);
    $.get(url).done(function (data) {
      if (notJSON(data)) {
        return;
      }
      var json = $.parseJSON(data);

      var count = Object.keys(json.data).length;

      var i;
      var j;
      var value = "";

      for (i = 0; i < count; i++) {
        for (j = 0; j < 6; j++) {
          if (j == 0) {
            value += '<tr><td style="font-size:medium;">' + json.data[i][j] + '</td>';
          } else if (j == 1) {
            value += '<td  style="font-size:medium;">' + json.data[i][j] + '</td>';
          } else if (j == 2) {
            value += '<td class="option_list" style="font-size:medium;">' + json.data[i][j] + '</td>';
          } else if (j == 3) {
            value += '<td  style="font-size:medium;">' + json.data[i][j] + '</td>';
          } else if (j == 4) {
            value += '<td class="option_list_status"  style="font-size:medium;">' + json.data[i][j] + '</td>';
          } else {
            value += '<td class="option_list_finish_date" style="font-size:medium;">' + json.data[i][j] + "</td></tr>";
          };
        }
      };

      // console.log(value);

      if (count != 0) {
        // Swal.fire({
        //   title: '查詢成功',
        //   showConfirmButton: false,
        //   timer: 1000
        // })

        $("#report_tel").html(json.report_tel);
        $("#id-wrapper").append('<center><table><tr style="font-size:large;"><th>合約編號</th><th>反映內容</th><th>反應者</th><th>反應者電話</th><th>保養員</th><th>保養員電話</th><tr/>' + value + '</table></center>');
        // alert("[報馬仔前五筆資訊]\n[時間] [說明] [進度] [完成日]\n" + value);
      } else {
        alert('無資料顯示!');
      };
    });

  }











});